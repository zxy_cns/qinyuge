package com.example.common.exception;

import com.example.utils.Result;
import com.example.utils.ResultUtil;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * 处理全局异常
 */
@ControllerAdvice
public class GlobalExceptionHandler {
    private final static Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
    @ExceptionHandler(Exception.class) //给controller添加异常处理，括号中指定要捕获处理哪种异常，Exception.class表示处理所有种类的异常
    @ResponseBody  //返回给浏览器显示出来
    public Result handler1(Exception e){ //如果要使用异常对象，可以作为参数传入异常对象
        if(e instanceof AuthorizationException){
            return ResultUtil.error(403,"权限不足");
        }else if (e.toString().contains("身份证号和批次")){
            return ResultUtil.error(500,"身份证号已被其他人报名使用");
        }else{
            logger.error("服务器内部异常：",e);
            return ResultUtil.error("服务器异常");
        }

    }


}
