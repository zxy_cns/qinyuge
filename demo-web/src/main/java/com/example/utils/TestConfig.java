package com.example.utils;

import com.example.common.TestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description:
 * @author: xxx
 * @create: 2021/3/27 13:50
 */
//@Configuration
public class TestConfig implements WebMvcConfigurer {
    @Autowired
    private TestInterceptor testInterceptor;
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(testInterceptor).addPathPatterns("/**").excludePathPatterns("/oauth/**","/studio/getById","/images/**");
    }

}
