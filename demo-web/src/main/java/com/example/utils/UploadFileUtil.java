package com.example.utils;

import com.example.shiro.UploadInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class UploadFileUtil {

    private static String signReplacePath;
    @Value("${file.replacePath}")
    private void setReplacePath(String replacePath){
        signReplacePath = replacePath;
    }
    public static List<UploadInfo> upload(List<MultipartFile> files, String uploadFilePath, String url) throws Exception {
        List<UploadInfo> list =new ArrayList<>();

        for (MultipartFile file : files) {
            UploadInfo uploadInfo = new UploadInfo();
            String originalFilename = "file"+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            String fileName = IDTool.getUUID32() + originalFilename;
            String fileType = fileName.substring(fileName.lastIndexOf(".") + 1);
            if (SystemConstant.ALLOW_UPLOAD_FILE_TYPE.contains(fileType)){
                long fileSize = file.getSize();
                File packageFile = new File(uploadFilePath);
                if (!packageFile.exists()) {
                    packageFile.mkdir();
                }
                File targetFile = new File(uploadFilePath + fileName);
                file.transferTo(targetFile);
                uploadInfo.setBeginFileName(originalFilename);
                uploadInfo.setLastFileName(fileName);
                uploadInfo.setFileType(fileType);
                uploadInfo.setFileSize(getPrintSize(fileSize));
                uploadInfo.setUploadUrl(targetFile.toString());
                String  uploadUrl = uploadInfo.getUploadUrl().replace(signReplacePath,url);
                uploadInfo.setResult("上传成功");

                uploadInfo.setUploadUrl(uploadUrl);
                list.add(uploadInfo);
            }else{
                throw new RuntimeException(fileType+"文件类型不允许上传");
            }

        }
        return list;
        }

    public static String getPrintSize(long size) {
        // 如果字节数少于1024，则直接以B为单位，否则先除于1024，后3位因太少无意义
        double value = (double) size;
        if (value < 1024) {
            return String.valueOf(value) + "B";
        } else {
            value = new BigDecimal(value / 1024).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
        }
        // 如果原字节数除于1024之后，少于1024，则可以直接以KB作为单位
        // 因为还没有到达要使用另一个单位的时候
        // 接下去以此类推
        if (value < 1024) {
            return String.valueOf(value) + "KB";
        } else {
            value = new BigDecimal(value / 1024).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
        }
        if (value < 1024) {
            return String.valueOf(value) + "MB";
        } else {
            // 否则如果要以GB为单位的，先除于1024再作同样的处理
            value = new BigDecimal(value / 1024).setScale(2, BigDecimal.ROUND_DOWN).doubleValue();
            return String.valueOf(value) + "GB";
        }
    }



}
