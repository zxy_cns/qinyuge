package com.example.utils;


import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class ResultUtil {
    public static Result success(Object object) {
        Result result = new Result();
        result.setCode(0);
        result.setMsg("成功");
        result.setData(object);
        return result;
    }
    public static Result successPage(Object object,Integer total) {
        Result result = new Result();
        result.setCode(0);
        result.setMsg("成功");
        Map<String,Object> map  = new HashMap<>();
        map.put("data",object);
        map.put("total",total);
        result.setData(map);
        return result;
    }
    public static Result successPageNew(Object object, Integer total, BigDecimal money) {
        Result result = new Result();
        result.setCode(0);
        result.setMsg("成功");
        Map<String,Object> map  = new HashMap<>();
        map.put("data",object);
        map.put("total",total);
        map.put("money",money);
        result.setData(map);
        return result;
    }
    public static Result success() {
        return success(null);
    }

    public static Result error(Integer code, String msg) {
        Result result = new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }
    public static Result error(String msg) {
        Result result = new Result();
        result.setCode(-9999);
        result.setMsg(msg);
        return result;
    }
}
