package com.example.utils;

/**
 * @description:
 * @author: xxx
 * @create: 2022/11/2 12:00
 */
import lombok.Data;
@Data
public class HwSmsResult {
    private String originTo;
    private String createTime;
    private String from;
    private String smsMsgId;
    private String status;
}