package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.GoodsService;
import com.example.service.GoodsStatusService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 申请商品封存
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("goodsStatus")
public class GoodsStatusController {

    @Autowired
    private GoodsStatusService service;
    @Autowired
    private GoodsService goodsService;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        jsonObject.put("userId",TokenTool.getUserId());
        jsonObject.put("statuss", "1,2");
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {
        JSONObject jsonObject = JSON.parseObject(json);
        jsonObject.put("userId",TokenTool.getUserId());
        jsonObject.put("statuss", "1,2");
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {
        return ResultUtil.success(service.getById(map));
    }
    /**
     * @description: 申请封存
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("save")
    @Transactional(rollbackFor = Exception.class)
    public Result save(@RequestBody Map<String,Object> map) {
        Map<String,Object> mapId = new HashMap<>();
        mapId.put("id",map.get("goodsId"));
        Map<String,Object> mapGoods = goodsService.getById(mapId);
        if (mapGoods.get("uId").toString().equals(TokenTool.getUserId())){
            if (mapGoods.get("added").toString().equals("1") || mapGoods.get("added").toString().equals("2")){
                map.put("id",IDTool.getUUID32());
                map.put("added",mapGoods.get("added").toString());
                map.put("userId",TokenTool.getUserId());
                service.save(map);
                Map<String,Object> mapNew = new HashMap<>();
                mapNew.put("id",map.get("goodsId"));
                mapNew.put("added",5);
                goodsService.update(mapNew);
                return ResultUtil.success();
            }else{
                return ResultUtil.error(-500,"商品状态不允许封存");
            }
        }else{
            return ResultUtil.error(-500,"非法操作");
        }

    }
    /**
     * @description: 确认收货
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("confirm")
    @Transactional(rollbackFor = Exception.class)
    public Result confirm(@RequestBody Map<String,Object> map) throws Exception {
        Map<String,Object> mapGoods = service.getById(map);
        if (mapGoods.get("status").toString().equals("2") && mapGoods.get("userId").toString().equals(TokenTool.getUserId())){
            map.put("status",4);
            service.update(map);
            Map<String,Object> mapEarnings = new HashMap<>();
            mapEarnings.put("goodsId",mapGoods.get("goodsId"));
            mapEarnings.put("userId",mapGoods.get("userId"));
            mapEarnings.put("status",2);
            mapEarnings.put("payStatus",1);
            mapEarnings.put("payStatusNew",3);
            goodsService.updateGoodsAddedEarnings(mapEarnings);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"状态异常");
        }
    }

}
