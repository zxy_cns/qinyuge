package com.example.utils;

import com.example.shiro.util.ShiroUtils;
import org.apache.shiro.session.Session;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public class TokenTool {

	/**
	 * 获取用户ID
	 * @return
	 */
    public static String getUserId() {

        Session session = ShiroUtils.getSession();
        System.out.println("id"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String userId = null;
        if(userInfo!=null){
            userId = userInfo.get("id").toString();
        }
        return userId;
    }
    /**
     * 获取用户ID
     * @return
     */
    public static Map<String, Object> getUserInfo() {

        Session session = ShiroUtils.getSession();
        System.out.println("id"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        if(userInfo!=null){
            return userInfo;
        }else{
            return null;
        }


    }
    /**
     *
     * 获取当前画室长拥有的画室Id集合
     * @return
     */
    public static String getHszStudioId() {

        Session session = ShiroUtils.getSession();
        System.out.println("getUserId"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String hszStudioIds = null;
        if(userInfo!=null){
            hszStudioIds = userInfo.get("hszStudioIds").toString();
        }
        return hszStudioIds;
    }
     /**
  	 * 获取用户角色所属
  	 * @return
  	 */
      public static List<Map<String, Object>> getRole() {

          Session session = ShiroUtils.getSession();
          System.out.println("sessionId"+session.getId());
          List<Map<String, Object>> roleList = (List<Map<String, Object>>) session.getAttribute("roleList");

          return roleList;
      }

    /**
     * getUrlById
     * 获取用户角色所属:m：超级管理员，j:机构管理员
     * @return
     */
    public static String getPermissionType() {

        Session session = ShiroUtils.getSession();
        System.out.println("getUserId"+session.getId());
        Map<String, Object>  userInfo = (Map<String, Object>) session.getAttribute("userInfo");
        String type = null;
        if(userInfo!=null){
            type = userInfo.get("type").toString();
        }
        return type;
    }


    /**
     * 获取request
     *
     * @return
     */
    public static HttpServletRequest getRequest() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes();
        return requestAttributes == null ? null : requestAttributes.getRequest();
    }


}
