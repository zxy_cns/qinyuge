package com.example.utils;


public class SystemConstant {
    /**
     * 允许上传的文件类型
     */
    public static final String  ALLOW_UPLOAD_FILE_TYPE = ".jpg,.png,.jpeg,.gif,.ppt,.pptx,.doc,.docx,.pdf,.txt,.xlsx,.xls,.zip,.rar,.mp4,.mp3,";
}
