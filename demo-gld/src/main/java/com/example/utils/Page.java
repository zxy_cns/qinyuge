package com.example.utils;

import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

/**
 * @description:
 * @author: xxx
 * @create: 2021/4/1 18:04
 */
public class Page implements Serializable {

    public static JSONObject getPage(JSONObject jsonObject){

        Object str = jsonObject.get("pageNum");
        //默认第一页
        if(str==null){
            str = "1";
        }
        int pageNum = Integer.parseInt(str.toString());
        //默认10条
        str = jsonObject.get("pageSize");
        if(str==null){
            str = "10";
        }
        int pageSize = Integer.parseInt(str.toString());
        int startIndex = (pageNum -1)* pageSize;
        jsonObject.put("pageNum",startIndex);

        return jsonObject;
    }

}
