package com.example.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @description:
 * @author: xxx
 * @create: 2021/3/27 13:50
 */
@Configuration
public class MyPicConfig implements WebMvcConfigurer {
    @Value("${server.upload.basepath}")
    private String basepath;
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/images/**").addResourceLocations(basepath);
    }

}
