package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.CommissionService;
import com.example.service.GoodsService;
import com.example.service.OrderService;
import com.example.service.UserService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @description: 商品
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("order")
public class OrderController {

    @Autowired
    private OrderService service;
    @Autowired
    private GoodsService goodsService;
    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private CommissionService commissionService;
    /**
     * @description: 确认订单
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/11 15:49
     */
    /**
     * @description: 确认订单
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/11 15:49
     */
    @RequestMapping("confirmOrder")
    @Transactional(rollbackFor = Exception.class)
    public Result confirmOrder(@RequestBody Map<String,Object> map) throws Exception {
        Map<String,Object> mapOrder = service.getById(map);
        if (!mapOrder.get("status").toString().equals("3") && !mapOrder.get("status").toString().equals("4")){
                map.put("status",3);
                service.update(map);
                Map<String,Object> mapNew = new HashMap<>();
                mapNew.put("id",mapOrder.get("goodsId"));
                mapNew.put("buyPrice",mapOrder.get("price"));
                mapNew.put("buyTime",mapOrder.get("createDate"));
                mapNew.put("uId",mapOrder.get("buyerId"));
                mapNew.put("added",2);
                goodsService.update(mapNew);
                Map<String,Object> mapCommissionLog = new HashMap<>();
                mapCommissionLog.put("id",IDTool.getUUID32());
                mapCommissionLog.put("orderId",mapOrder.get("id"));
                mapCommissionLog.put("userId",mapOrder.get("buyerId"));
                BigDecimal money = new BigDecimal(Double.valueOf(mapOrder.get("price").toString()));//交易金额
                BigDecimal td = new BigDecimal(Double.valueOf(0.001));//提点
                BigDecimal tdMoney = money.multiply(td).setScale(2, BigDecimal.ROUND_HALF_UP);//乘法四舍五入保留两位小数
                mapCommissionLog.put("money",tdMoney);
                commissionService.saveCommissionLog(mapCommissionLog);
                Map<String,Object> mapUserId = new HashMap<>();
                mapUserId.put("id",mapOrder.get("buyerId"));
                Map<String,Object> mapUser =  userService.getById(mapUserId);
                Map<String,Object> mapCommission = new HashMap<>();
                mapCommission.put("id",mapUser.get("pid"));
                Map<String,Object> commission = commissionService.getById(mapCommission);
                Double commissionMoney = Double.parseDouble(commission.get("money").toString());
                mapCommission.put("money",new BigDecimal(commissionMoney).add(tdMoney).setScale(2, BigDecimal.ROUND_HALF_UP));
                mapCommission.put("uuid",commission.get("uuid"));
                mapCommission.put("uuidNew",IDTool.getUUID32());
                Integer result = commissionService.update(mapCommission);
                Map<String,Object> mapEarnings = new HashMap<>();
                mapEarnings.put("goodsId",mapOrder.get("goodsId"));
                mapEarnings.put("userId",mapOrder.get("userId"));
                mapEarnings.put("status",2);
                mapEarnings.put("payStatus",1);
                mapEarnings.put("payStatusNew",2);
                goodsService.updateGoodsAddedEarnings(mapEarnings);
                if(result <= 0){
                    throw new Exception("更新失败");
                }
                return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"订单已完成,请勿重复操作");
        }


    }
    /**
     * @description: 取消订单
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/12 17:13
     */
    @RequestMapping("cancelOrder")
    public Result cancelOrder(@RequestBody Map<String,Object> map) {
        Map<String,Object> mapOrder = service.getById(map);
        if (mapOrder.get("status").toString().equals("1") ||
                mapOrder.get("status").toString().equals("2")){
            Map<String,Object> mapNew = new HashMap<>();
            mapNew.put("id",mapOrder.get("id"));
            mapNew.put("status","4");
            mapNew.put("goodsId",mapOrder.get("goodsId"));
            service.update(mapNew);
            return ResultUtil.success();
        }else{
            return ResultUtil.error(-500,"该状态不允许取消订单");
        }
    }
    /**
     * @description: 确认提现
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/11 15:52
     */
    @RequestMapping("confirmWithdraw")
    @Transactional(rollbackFor = Exception.class)
    public Result confirmWithdraw(@RequestBody Map<String,Object> map) throws Exception {
        Map<String,Object> mapWithdraw = commissionService.getWithdrawById(map);
        if(TokenTool.getPermissionType().equals("t")){
            map.put("status",2);
            if(mapWithdraw.get("status").toString().equals("1")){
                commissionService.updateWithdraw(map);
                BigDecimal money = new BigDecimal(Double.valueOf(mapWithdraw.get("money").toString()));//提现金额
                Map<String,Object> mapCommission = new HashMap<>();
                mapCommission.put("id",mapWithdraw.get("userId"));
                Map<String,Object> commission = commissionService.getById(mapCommission);
                Double commissionMoney = Double.parseDouble(commission.get("money").toString());
                Double commissionOldMoney = Double.parseDouble(commission.get("oldMoney").toString());
                mapCommission.put("money",new BigDecimal(commissionMoney).subtract(money).setScale(2, BigDecimal.ROUND_HALF_UP));
                mapCommission.put("oldMoney",new BigDecimal(commissionOldMoney).add(money).setScale(2, BigDecimal.ROUND_HALF_UP));
                mapCommission.put("uuid",commission.get("uuid"));
                mapCommission.put("uuidNew",IDTool.getUUID32());
                Integer result = commissionService.update(mapCommission);
                if(result <= 0){
                    throw new Exception("更新失败");
                }
            }else{
                return ResultUtil.error(-500,"该状态不允许修改");
            }
        }
        return ResultUtil.success();
    }
    /**
     * @description: 取消提现
     * @param map
     * @return: com.example.utils.Result
     * @author: xxx
     * @time: 2022/6/13 11:34
     */
    @RequestMapping("cancelWithdraw")
    public Result cancelWithdraw(@RequestBody Map<String,Object> map) {

        Map<String,Object> mapWithdraw = commissionService.getWithdrawById(map);
        if(TokenTool.getPermissionType().equals("t")){
            map.put("status",3);
            if(mapWithdraw.get("status").toString().equals("1")){
                commissionService.updateWithdraw(map);
            }else{
                ResultUtil.error(-500,"该状态不允许取消");
            }
        }
        return ResultUtil.success();
    }
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")) {
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        if(TokenTool.getPermissionType().equals("t")) {
            jsonObject.put("hszStudioIds",TokenTool.getHszStudioId());
        }
        return ResultUtil.success(service.getList(jsonObject));
    }
    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {

        return ResultUtil.success(service.getById(map));
    }

    /**
     * @description: 分页每天的成交流水总和
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getOrderCount")
    public Result getOrderCount(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        List<Map<String, Object>> list = service.getOrderCount(jsonObject);
        int total = service.getOrderCounts(jsonObject);

        return ResultUtil.successPage(list,total);
    }

    @RequestMapping("delete")
    public Result delete(@RequestBody Map<String,Object> map) {
        service.delete(map);
        return ResultUtil.success();
    }
}
