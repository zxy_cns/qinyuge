package com.example.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.example.service.BannerService;
import com.example.service.NewService;
import com.example.service.UserService;
import com.example.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 资讯
 * @author: xxx
 * @create: 2021/7/19 09:30
 */
@RestController
@RequestMapping("new")
public class NewController {

    @Autowired
    private NewService service;
    @Autowired
    private UserService userService;
    /**
     * @description: 分页
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/7/19 09:30
     */
    @RequestMapping("getPage")
    public Result getPage(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        Page.getPage(jsonObject);
        jsonObject.put("userId", TokenTool.getUserId());
//        jsonObject.put("userId", "12573e1d9c704db1b3c403c787c5c468");
        List<Map<String, Object>> list = service.getList(jsonObject);
        int total = service.getCount(jsonObject);

        return ResultUtil.successPage(list,total);
    }
    /**
     * @description: 列表
     * @param json
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getList")
    public Result getList(@RequestBody String json) {

        JSONObject jsonObject = JSON.parseObject(json);
        jsonObject.put("userId", TokenTool.getUserId());
//        jsonObject.put("userId", "12573e1d9c704db1b3c403c787c5c468");
        return ResultUtil.success(service.getList(jsonObject));
    }

    /**
     * @description: 详情
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("getById")
    public Result getById(@RequestBody Map<String,Object> map) {
        Map<String,Object> mapNew = service.getById(map);
        if (mapNew.get("status").toString().equals("1")){
            mapNew.put("status",2);
            service.update(mapNew);
        }
        return ResultUtil.success(mapNew);
    }
    /**
     * @description: 保存
     * @param map
     * @return: com.example.common.entity.Result
     * @author: xxx
     * @time: 2021/5/06 09:30
     */
    @RequestMapping("save")
    public Result save(@RequestBody Map<String,Object> map) {
            List<Map<String,Object>> mapList = new ArrayList<>();
            List<Map<String,Object>> listNew = userService.getListNew(new JSONObject());
            String[] studioIds = map.get("studioIds").toString().split(",");
            for (String studioId : studioIds) {
                for (Map<String, Object> objectMap : listNew) {
                    if (objectMap.get("studioIds").toString().contains(studioId)){
                        Map<String,Object> mapNew = new HashMap<>();
                        mapNew.put("id", IDTool.getUUID32());
                        mapNew.put("userId", objectMap.get("id").toString());
                        mapNew.put("title", map.get("title").toString());
                        mapNew.put("content", map.get("content").toString());
                        mapList.add(mapNew);
                    }
                }
            }

            service.save(mapList);
            return ResultUtil.success();
    }

}
