package com.example.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * @description:
 * @author: xxx
 * @create: 2021/7/19 17:24
 */
@Mapper
public interface OrderMapper {

    List<Map<String, Object>> getList(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getWeekList(@Param("jsonObject") JSONObject jsonObject);
    Integer getWeekCount(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getListPrice(List<String> ids);
    List<Map<String, Object>> getFansListBuy(List<String> ids);
    List<Map<String, Object>> getFansListSale(List<String> ids);
    List<Map<String, Object>> getCommissionList(@Param("jsonObject") JSONObject jsonObject);
    Integer getCommissionCount(@Param("jsonObject") JSONObject jsonObject);
    List<Map<String, Object>> getListSale(@Param("jsonObject") JSONObject jsonObject);

    Integer getCount(@Param("jsonObject") JSONObject jsonObject);
    Integer getCountSale(@Param("jsonObject") JSONObject jsonObject);
    Integer getTodayCount(Map<String, Object> map);
    Map<String, Object> getSumMoney(Map<String, Object> map);
    List<Map<String, Object>> getOrderCount(@Param("jsonObject") JSONObject jsonObject);
    Integer getOrderCounts(@Param("jsonObject") JSONObject jsonObject);
    Map<String, Object> getById(Map<String, Object> map);
    Map<String, Object> getOrderAddById(String id);
    Map<String, Object> getOrderAddByGoodsAddId(Map<String, Object> map);
    void saveOrderAdd(Map<String, Object> map);
    void saveOrderAddLog(Map<String, Object> map);
    void save(Map<String, Object> map);
    void update(Map<String, Object> map);
    void updateOrderAdd(Map<String, Object> map);
    void delete(Map<String, Object> map);
}
