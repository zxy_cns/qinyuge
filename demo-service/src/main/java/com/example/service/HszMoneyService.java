package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.BannerMapper;
import com.example.mapper.HszMoneyMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: 画室长收入
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class HszMoneyService {
    @Autowired
    private HszMoneyMapper mapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){

        return mapper.getList(jsonObject);
    }
    public Integer getCount(JSONObject jsonObject){

        return mapper.getCount(jsonObject);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return mapper.getById(map);
    }
    public void save(Map<String,Object> map){
        mapper.save(map);
    }
    public Integer update(Map<String,Object> map){
        return mapper.update(map);
    }
    public void delete(Map<String,Object> map){
        mapper.delete(map);
    }

}
