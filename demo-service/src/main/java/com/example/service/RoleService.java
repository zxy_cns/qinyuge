package com.example.service;

import com.alibaba.fastjson.JSONObject;
import com.example.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @description: 权限
 * @author: xxx
 * @create: 2021/5/08 18:34
 */
@Service
public class RoleService {
    @Autowired
    private RoleMapper roleMapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){
        return roleMapper.getList(jsonObject);
    }
    public Integer getCount(JSONObject jsonObject){
        return roleMapper.getCount(jsonObject);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return roleMapper.getById(map);
    }
    public void save(Map<String,Object> map){
        roleMapper.save(map);
    }
    public void update(Map<String,Object> map){
        roleMapper.update(map);
    }
    public void delete(Map<String,Object> map){
        roleMapper.delete(map);
    }

}
