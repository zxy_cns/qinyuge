package com.example.service;

import com.alibaba.fastjson.JSONObject;

import com.example.mapper.GoodsMapper;
import com.example.mapper.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @description: 商品
 * @author: xxx
 * @create: 2021/7/19 17:34
 */
@Service
public class GoodsService {
    @Autowired
    private GoodsMapper mapper;
    @Autowired
    private OrderMapper orderMapper;

    public List<Map<String, Object>> getList(JSONObject jsonObject){
        jsonObject.put("del","1");
        return mapper.getList(jsonObject);
    }

    public List<Map<String, Object>> getTimeList(JSONObject jsonObject){
        return mapper.getTimeList(jsonObject);
    }
    public List<Map<String, Object>> getPreSale(Map<String, Object> map){
        return mapper.getPreSale(map);
    }
    public List<Map<String, Object>> getListSale(JSONObject jsonObject){
        return mapper.getListSale(jsonObject);
    }
    public List<Map<String, Object>> getListGoodsAdded(JSONObject jsonObject){
        return mapper.getListGoodsAdded(jsonObject);
    }
    public List<Map<String, Object>> getListGoodsAddedEarnings(JSONObject jsonObject){
        return mapper.getListGoodsAddedEarnings(jsonObject);
    }
    public Integer getCountGoodsAddedEarnings(JSONObject jsonObject){

        return mapper.getCountGoodsAddedEarnings(jsonObject);
    }
    public Integer getCount(JSONObject jsonObject){
        jsonObject.put("del","1");
        return mapper.getCount(jsonObject);
    }
    public Integer getCountSale(JSONObject jsonObject){

        return mapper.getCountSale(jsonObject);
    }
    public Integer getCountGoodsAdded(JSONObject jsonObject){

        return mapper.getCountGoodsAdded(jsonObject);
    }
    public Map<String, Object> getById(Map<String,Object> map){
        return mapper.getById(map);
    }
    public Map<String, Object> getGoodsAddedById(Map<String,Object> map){
        return mapper.getGoodsAddedById(map);
    }
    public Map<String, Object> getTimeById(Map<String,Object> map){
        return mapper.getTimeById(map);
    }
    public Integer getTimeByNow(){

        return mapper.getTimeByNow();
    }

    public Map<String, Object> getEarningsToday(Map<String,Object> map){
        return mapper.getEarningsToday(map);
    }
    public Map<String, Object> getEarningsAll(Map<String,Object> map){
        return mapper.getEarningsAll(map);
    }
    public void save(Map<String,Object> map){
        mapper.save(map);
    }
    public void saveGoodsAdded(Map<String,Object> map){
        mapper.saveGoodsAdded(map);
    }
    public void saveOrderAdd(Map<String,Object> map){
        orderMapper.saveOrderAdd(map);
    }
    public Integer updateGoodsAddedEarnings(Map<String,Object> map){
        return mapper.updateGoodsAddedEarnings(map);
    }

    public void update(Map<String,Object> map){
        mapper.update(map);
    }
    public void updateTime(Map<String,Object> map){
        mapper.updateTime(map);
    }
    public void updateGoodsAdded(Map<String,Object> map){
        mapper.updateGoodsAdded(map);
    }
    public void delete(Map<String,Object> map){
        mapper.delete(map);
    }


}
